# CPRC Example Theme

This repo is an example of how to create and register css themes with the [Cyberpunk RED - CORE](https://gitlab.com/cyberpunk-red-team/fvtt-cyberpunk-red-core) system for FoundryVTT.

While this repo could be used as a theme within CPRC it will not be maintained as a working themes. It serves only as an example of the css layout and method of registering a theme with the System's settings.

For full information see the [CSS Themes](https://gitlab.com/cyberpunk-red-team/fvtt-cyberpunk-red-core/-/wikis/System-Documentation/CSS-Themes) page on our wiki.

